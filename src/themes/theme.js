import {createTheme} from "@mui/material/styles";

export const theme = createTheme({
    components: {
        MuiCssBaseline: {
            styleOverrides: {
                body: {
                    background: '#EEE',
                    '.margin': {
                        margin: 50
                    },

                }
            }
        },
        MuiButton: {
            styleOverrides: {
                root: {
                    background: 'yellow'
                },
                outlined: {
                    background: 'red',
                    ':hover': {
                        background: 'red',
                    }
                }

            },
            defaultProps: {
                disableRipple: true
            },
            variants: [
                {
                    props: {variant: 'mainButton', type: 'number'}, // variant: mainButton && type: number
                    style: {
                        color: '#fff',
                        backgroundColor: '#50C2C9',
                        borderRadius: '20px',
                        textAlign: 'center',
                        height: 60,
                        '&:hover': {
                            backgroundColor: '#50C2C9',
                        }
                    }
                },

            ]
        }
    },
    dir: 'rtl',
    palette: {
        primary: {
            main: '#50C2C9',
            light: '#94F6FC'
        },
        background: '#EEE'
    },
    typography: {
        fontFamily: 'Poppins',
        h1: {
            fontSize: 18,
            fontWeight: 700,
            lineHeight: '27px'
        },
        body: {
            fontWeight: 400,
            fontSize: 13,
            lineHeight: '20px',
        },
    },

    customWidth: 500,
    spacing: 3

})