import Grid from "@mui/material/Grid";
import {useMediaQuery, useTheme} from "@mui/material";
import CustomPaginationActionsTable from "./components/CustomPaginationActionsTable";

function App() {
    const theme = useTheme()
    // const media = useMediaQuery(theme.breakpoints.up(750))
    return (
        <Grid container justifyContent={"center"} sx={{padding: 50}}>
            {/*<Grid container item justifyContent={"center"}>*/}
            {/*    <img src={image}/>*/}
            {/*</Grid>*/}
            {/*<Grid container item direction={'column'} alignItems={"center"}>*/}
            {/*    <Typography variant={'h1'} sx={{marginTop: '150px'}}>*/}
            {/*        Gets things done with TODo*/}
            {/*    </Typography>*/}
            {/*    <Typography variant={'body'} width={150}>*/}
            {/*        Lorem ipsum dolor sit amet, consectetur adipisicing elit. Adipisci alias iure obcaecati ratione voluptates voluptatum!*/}
            {/*    </Typography>*/}
            {/*</Grid>*/}
            {/*<Grid container item justifyContent={"center"} xs={3}>*/}
            {/*    <Button variant={'mainButton'} sx={{width: '100%'}}>*/}
            {/*        Get Started*/}
            {/*    </Button>*/}
            {/*</Grid>*/}

            {/*<StyledTextField*/}
            {/*    multiline={true}*/}
            {/*    rows={7}*/}
            {/*    type={'text'}*/}
            {/*    placeholder={'Enter your full name'}*/}
            {/*    sx={{width: 325}}*/}
            {/*/>*/}

            {/* Grid, Stack, typography, Box */}
            {/* margin, padding => num * 8 */}
            {/* borderRadius => num * 4 */}
            {/*<Grid container item justifyContent={"center"} bgcolor={'red'} m={10}>*/}
            {/*    Lorem ipsum dolor sit amet, consectetur adipisicing elit. Dicta, error?*/}
            {/*</Grid>*/}


            {/*<Button variant={'mainButton'} sx={(theme) => ({*/}
            {/*    border: 1*/}
            {/*})}>*/}
            {/*    Click*/}
            {/*</Button>*/}

            {/*<Button sx={{*/}
            {/*    background: media ? 'red': 'green'*/}
            {/*}}>Click me!</Button>*/}

            <CustomPaginationActionsTable/>


        </Grid>
    );
}

export default App;
