import {styled, TextField} from "@mui/material";


const StyledTextField = styled(TextField)(({theme}) => ({
    width: '100%',
    background: 'white',
    height: 50,
    borderRadius: '100px',
    // '& .MuiOutlinedInput-input': {
    //    padding: 0
    // },
    'label': {
        ...theme.typography['body'],
        color: 'rgba(0, 0, 0, 0.79)',
        // marginTop: -10,
        marginLeft: 35
    },

    'label.Mui-focused': {
        color: 'black',
        marginLeft: 0
    },

    // '& .MuiOutlinedInput-root': {
    //     borderRadius: '100px',
    //     height: 50,
    //     '& fieldset': {
    //         border: 0
    //     },
    //     '&.Mui-focused fieldset': {
    //         border: '1px solid black'
    //     },
    // }

    'fieldset': {
        border: 0
    },
}))


export default StyledTextField